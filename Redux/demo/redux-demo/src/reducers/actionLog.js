import { append, assoc } from 'ramda';
const initialState = {
  actions: [],
  showActionLog: false
};

export const ACTION_LOG_SHOW = 'ACTION_LOG_SHOW';
export const showActionLog = () => ({
  type: ACTION_LOG_SHOW
});

function appendActionLog(action, state) {
  return assoc('action', append(action, state.actions), state);
}

export function actionLog(state = initialState, action) {
  console.log(`Action ${action.type} recieved by the action log!`);
  switch(action.type) {
  case ACTION_LOG_SHOW: {
    return assoc('showActionLog', true, state);
  }
    default: return appendActionLog(action, state);
  }
};

